extends Node2D

var _consumed := false

func _on_Area2D_body_entered(_body) -> void:
	if not _consumed:
		_consumed = true
		($AnimationPlayer as AnimationPlayer).play("Consumed")
		var audio_player: AudioStreamPlayer2D = $AudioStreamPlayer2D
		if not audio_player.is_playing():
			audio_player.play()
		get_tree().call_group("GameState", "score_coin")
