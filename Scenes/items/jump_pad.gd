extends Area2D

const Player: Script = preload("res://scenes/player/player.gd")


func _on_JumpPad_player_entered(player: Player) -> void:
	($AnimationPlayer as AnimationPlayer).play("Sprung")
	player.boost()
