extends Node2D

const Player: Script = preload("res://scenes/player/player.gd")
export(String, FILE, "*tscn") var path_to_game_over_scene := "res://scenes/levels/game_over.tscn"
export(String, FILE, "*tscn") var path_to_victory_scene := "res://scenes/levels/victory.tscn"
export(int, 1, 10) var lives_left := 3
export(int, 5, 20) var coins_for_new_life := 10
export var player := @"./Player"
var _coin_count := 0


func _ready() -> void:
	_update_life_count()
	_update_coin_count()
#	var __ := DebugOverlay.add_monitor(
#		"Player lives_left left", self, ".:lives_left"
#	)


func get_hurt() -> void:
	lives_left -= 1
	(get_node(player) as Player).get_hurt()
	_update_life_count()

	if lives_left < 0:
		end_game()


func score_coin() -> void:
	_coin_count += 1
	_update_coin_count()

	if (_coin_count % coins_for_new_life) == 0:
		lives_left += 1
		_update_life_count()


func end_game() -> void:
	if get_tree().change_scene(path_to_game_over_scene) != OK:
		printerr("Error getting %s" % path_to_game_over_scene)


func win_game() -> void:
	if get_tree().change_scene(path_to_victory_scene) != OK:
		printerr("Error getting %s" % path_to_victory_scene)


func _update_life_count() -> void:
	get_tree().call_group("GUI", "update_life_count", lives_left)


func _update_coin_count() -> void:
	get_tree().call_group("GUI", "update_coin_count", _coin_count)
