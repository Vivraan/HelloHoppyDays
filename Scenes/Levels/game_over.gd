extends Control

export(String, FILE, "*.tscn") var path_to_level := "res://scenes/levels/Level1.tscn"


func _on_RestartButton_pressed() -> void:
	if get_tree().change_scene(path_to_level) != OK:
		printerr("Error loading %s" % path_to_level)
