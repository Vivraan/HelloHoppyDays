extends AnimatedSprite


func _on_Player_moved(motion: Vector2) -> void:
	var anim_state := "Idle"

	if motion.y < 0.0:
		anim_state = "Jumping"
	elif motion.x != 0.0:
		anim_state = "Walking"

	flip_h = motion.x < 0.0
	play(anim_state)


func _on_Player_hurt() -> void:
	# TODO doesn't work yet since player jumps when hurt
	play("Hurt")
