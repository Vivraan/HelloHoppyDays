extends KinematicBody2D

signal moved
signal hurt
const PIXELS_PER_UNIT := 100
const FALL_LIMIT := 40.0 * PIXELS_PER_UNIT
export(float, 1, 50) var move_speed := 10.0
export(float, 1, 50) var gravity := 5.0
export(float, 1, 50) var jump_speed := 30.0
export(float, 1, 10, .5) var boost_multiplier := 2.0
var _motion := Vector2.ZERO


func _ready() -> void:
	move_speed *= PIXELS_PER_UNIT
	gravity *= PIXELS_PER_UNIT
	jump_speed *= PIXELS_PER_UNIT
	# DebugOverlay.visible = true
	# var __ := DebugOverlay.add_monitor(
	# 	"Player Position", self, "", "_get_position_in_units")


func _physics_process(_delta: float) -> void:
	_apply_gravity()
	_jump()
	_move_sideways()
	emit_signal("moved", _motion)
	var __ = move_and_slide(_motion, Vector2.UP)

#
#func _unhandled_input(event: InputEvent) -> void:
#	match event.get_class():
#		"InputEventAction":
#			_jump()
#			_move_sideways()


func get_hurt() -> void:
	emit_signal("hurt")

	# so that is_on_floor() returns false, we shift it and pause for a frame
	# TODO it's buggy! Fix this later.
	position.y -= 1
	yield(get_tree(), "idle_frame")

	_motion.y = -jump_speed
	($HurtSFX as AudioStreamPlayer).play()


func boost() -> void:
	# so that is_on_floor() returns false, we shift it and pause for a frame
	# TODO it's buggy! Fix this later.
	position.y -= 1
	yield(get_tree(), "idle_frame")

	# this adds extra force to a jump when on a jump pad
	_motion.y = -jump_speed * boost_multiplier
	($JumpSFX as AudioStreamPlayer).play()


func _get_position_in_units() -> Vector2:
	return position / PIXELS_PER_UNIT


func _apply_gravity() -> void:
	if position.y > FALL_LIMIT:
		get_tree().call_group("GameState", "end_game")

	if is_on_floor() and _motion.y > 0.0:
		_motion.y = 0.0
	# when touching the ceiling, move down by a pixel to prevent hovering
	elif is_on_ceiling():
		_motion.y = 1.0
	else:
		_motion.y += gravity


func _jump() -> void:
	if Input.is_action_pressed("player_jump") and is_on_floor():
		_motion.y = -jump_speed
		($JumpSFX as AudioStreamPlayer).play()


func _move_sideways() -> void:
	var left_pressed := Input.is_action_pressed("player_left")
	var right_pressed := Input.is_action_pressed("player_right")
	var both_pressed := left_pressed and right_pressed
	var neither_pressed := not (left_pressed or right_pressed)

	if left_pressed:
		_motion.x = -move_speed

	if right_pressed:
		_motion.x = move_speed

	if both_pressed or neither_pressed:
		_motion.x = 0.0

