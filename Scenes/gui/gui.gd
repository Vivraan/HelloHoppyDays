extends CanvasLayer

onready var _life_count: Label = $Control/TextureRect/HBoxContainer/LifeCount
onready var _coin_count: Label = $Control/TextureRect/HBoxContainer/CoinCount


func update_life_count(lives_left: int) -> void:
	_life_count.text = str(lives_left)


func update_coin_count(coin_count: int) -> void:
	_coin_count.text = str(coin_count)
