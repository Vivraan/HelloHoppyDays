extends Node2D

const Player: Script = preload("res://scenes/player/player.gd")
const PIXELS_PER_UNIT := 100
export(float, 1, 10) var speed := 2.0
onready var _area_2d: Area2D = $Area2D

func _ready() -> void:
	speed *= PIXELS_PER_UNIT
	set_as_toplevel(true)
	global_position = (get_parent() as Node2D).global_position


func _process(delta: float) -> void:
	position.y += speed * delta
	var hit_bodies = _area_2d.get_overlapping_bodies()
	for body in hit_bodies:
		if body is Player:
			get_tree().call_group("GameState", "get_hurt")
		queue_free()

func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()
