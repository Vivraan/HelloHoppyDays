extends Node2D

const LightningScene: PackedScene = preload("res://scenes/hazards/Lightning.tscn")
var _timeout := false
onready var _raycast: RayCast2D = $Sprite/RayCast2D
onready var _timer: Timer = $Sprite/Timer


func _process(_delta: float) -> void:
	if _raycast.is_colliding():
		_fire()


func _fire() -> void:
	if not _timeout:
		_raycast.add_child(LightningScene.instance())
		_timer.start()
		_timeout = true


func _on_Timer_timeout() -> void:
	_timeout = false
